// Preloader // 
window.onload = function () {
  document.body.classList.add('loaded_hiding');
  window.setTimeout(function () {
    document.body.classList.add('loaded');
    document.body.classList.remove('loaded_hiding');
  }, 1500);
}

// Global Variables // 
const mainContent = document.querySelector(".main__content");

// Overlay Settings // 
const overlay = document.querySelector('.overlay');

const overlaAсtive = function() {
  overlay.classList.add('overlay--active');
}

const overlaDelete = function() {
  overlay.classList.remove('overlay--active');
}

const overlayHide = function () {
  overlay.classList.remove('overlay--catalog');
  overlay.classList.remove('overlay--menu');
}

// Cabinet - Switching Tabs  //
const tabs = document.querySelectorAll(".cabinet__tab");
const tabContent = document.querySelectorAll(".cabinet-block");
const tabsShow = document.querySelector('.cabinet__wrapper__button');
const cabinetAside = document.querySelector('.cabinet__aside');
const cabinetInformation = document.querySelector('.cabinet__information')
const tabData = document.querySelector('.data__tab');
const tabFavorites = document.querySelector('.favorites__tab');
const datPage = document.querySelector('.dat__page');
const favoritesPage = document.querySelector('.favorites__page');
const anchor = location.hash;

const onClickHandlerTab = (currentTab) => {
  const type = currentTab.dataset.type;
  const activeClass = 'cabinet-block--active';

  tabs.forEach(tab => tab.classList.remove('tab-active'));
  currentTab.classList.add('tab-active');

  document.querySelector(`.${activeClass}`).classList.remove(activeClass);
  document.getElementById(type).classList.add(activeClass);
}

const cabinetAsideHide = function() {
  cabinetAside.classList.add('cabinet__aside--hide');
  tabsShow.classList.add('cabinet__wrapper__button--active');
  cabinetInformation.classList.remove('cabinet__information--hide')
}

const cabinetAsideShow = function() {
  cabinetAside.classList.remove('cabinet__aside--hide');
  tabsShow.classList.remove('cabinet__wrapper__button--active');
  cabinetInformation.classList.add('cabinet__information--hide')
}

if (anchor) {
  const tab = document.querySelector(`[data-type='${anchor.slice(1)}']`);

  if (tab) {
    onClickHandlerTab(tab);
  }
}

let tabNo = 0;
let contentNo = 0;

tabs.forEach((tab) => {
  tab.dataset.id = tabNo;
  tabNo++;
  tab.addEventListener("click", function () {
    onClickHandlerTab(this);

    if (window.matchMedia("(max-width: 576px)").matches) {
      cabinetAsideHide();
    }
  });
});

if (mainContent.classList.contains('cabinet__content')) {
  tabsShow.addEventListener('click', function() {
    cabinetAsideShow();
  })

  if (tabData.classList.contains('tab-active')) {
    datPage.classList.add('page--active');
  }
  
  if (tabFavorites.classList.contains('tab-active')) {
    favoritesPage.classList.add('page--active');
  } 
}

tabContent.forEach((content) => {
  content.dataset.id = contentNo;
  contentNo++;
});

datPage.addEventListener('click', function () {
  window.location.replace('cabinet.html#data-id');
    if (mainContent.classList.contains("cabinet__content")) {
      location.reload();
    }
})

favoritesPage.addEventListener('click', function () {
  window.location.replace('cabinet.html#favorites-id');
  if (mainContent.classList.contains("cabinet__content")) {
    location.reload();
  }
})

// Mobile sidebar & menu 1360px settings // 
const catalogButton = document.querySelector('.mobile-nav__catalog');
const catalogList = document.querySelector('.sidebar__navigation');
const menuButton = document.querySelector('.mobile-nav__menu');
const menuList = document.querySelector('.mobile__menu');

const openSideBar = function () {
  catalogButton.classList.toggle('mobile-nav__catalog--active');
  catalogList.classList.toggle('sidebar__navigation--show');
  overlay.classList.toggle('overlay--catalog');
}

const closeSideBar = function () {
  catalogButton.classList.remove('mobile-nav__catalog--active');
  catalogList.classList.remove('sidebar__navigation--show');
  overlay.classList.remove('overlay--catalog');
}

const openMenu = function () {
  menuButton.classList.toggle('mobile-nav__menu--active');
  menuList.classList.toggle('mobile__menu--show');
  overlay.classList.toggle('overlay--menu');
}

const closeMenu = function () {
  menuButton.classList.remove('mobile-nav__menu--active');
  menuList.classList.remove('mobile__menu--show');
  overlay.classList.remove('overlay--menu');
}

catalogButton.addEventListener("click", function (e) {
    e.stopPropagation();
    openSideBar();
    closeMenu(); 
});

menuButton.addEventListener("click", function (e) {
  e.stopPropagation();
  openMenu();
  closeSideBar();
});

document.addEventListener("click", function (e) {
  const target = e.target;
  const its_catalogList = target == catalogList || catalogList.contains(target);
  const its_catalogButton = target == catalogButton;
  const catalog_is_active = catalogList.classList.contains("sidebar__navigation--show");
  const its_menuList = target == menuList || menuList.contains(target);
  const its_menuButton = target == menuButton;
  const menu_is_active = menuList.classList.contains("mobile__menu--show");
  
  if (!its_catalogList && !its_catalogButton && catalog_is_active) {
    openSideBar();
    overlayHide();
  }  

  if (!its_menuList && !its_menuButton && menu_is_active) {
    openMenu();
    overlayHide();
  }
});

// header //
// dropdown menu // 
const dropdownItem = document.querySelector(".dropdown__item");
const dropdownList = document.querySelector(".dropdown-list")

const dropdownOpen = function () {
  dropdownList.classList.toggle("dropdown-list--show");
  dropdownItem.classList.toggle("dropdown__item--active");
}

dropdownItem.addEventListener("click", function (e) {
    e.stopPropagation();
    dropdownOpen();
});

document.addEventListener("click", function (e) {
  const target = e.target;
  const its_menu = target == dropdownList || dropdownList.contains(target);
  const its_btnMenu = target == dropdownItem;
  const menu_is_active = dropdownList.classList.contains("dropdown-list--show");

  if (!its_menu && !its_btnMenu && menu_is_active) {
    dropdownOpen();
  }
});

// Mobile sidebar 1560px settings //
const accordionMenu = document.querySelector('.accordion__menu');

const fullSideBar = function () {
  catalogList.classList.remove('mini-sidebar__navigation');
}

accordionMenu.addEventListener("click", function (e) {
  e.stopPropagation();
  fullSideBar();
});

document.addEventListener("click", function (e) {
    if (e.target.className != ".accordion__menu") {
      catalogList.classList.add('mini-sidebar__navigation');
    };
});


// filters settings/ 
if (mainContent.classList.contains("caralog__content")) {

const filterList = document.querySelector('.filters__aside');
const filterOpenButton = document.querySelector('.filters__button');
const filterCloseButton = document.querySelector('.сlose-filters__button');

const filterShow = function () {
  filterList.classList.toggle('filters__aside--show');
  overlay.classList.toggle('overlay--active');
}

const filterHide = function () {
  filterList.classList.remove('filters__aside--show');
  overlay.classList.remove('overlay--active');
}

filterOpenButton.addEventListener('click', function (e) {
  e.stopPropagation();
  filterShow();
});

filterCloseButton.addEventListener('click', function (e) {
  e.stopPropagation();
  filterHide();
});

function uncheckAll2() {
  var inputs = document.querySelectorAll('.filter-checkbox');
  for(var i = 0; i < inputs.length; i++) {
    inputs[i].checked = false;
  }
}

}

// modal window offer-price // 
if (mainContent.classList.contains("сard-product__content")) {

  const offerPriceModal = document.querySelector(".offer-price__modal");
  const offerPriceOpen = document.querySelector(".offer-your-price__button");
  const offerPriceClose = document.querySelector(".offer-price__close");

  const offerPriceShow = function () {
    offerPriceModal.classList.toggle('offer-price__modal--show');
  }

  const offerPriceHide = function () {
    offerPriceModal.classList.remove('offer-price__modal--show');
  }

  offerPriceOpen.addEventListener("click", function (e) {
      e.stopPropagation();
      offerPriceShow();
      overlaAсtive();
  });

  offerPriceClose.addEventListener("click", function (e) {
      e.stopPropagation();
      offerPriceHide();
      overlaDelete();
  });

  document.addEventListener("click", function (e) {
    const target = e.target;
    const its_offerModal = target == offerPriceModal || offerPriceModal.contains(target);
    const its_offerBtn = target == offerPriceOpen;
    const offer_is_active = dropdownList.classList.contains("offer-price__modal--show");

    if (!its_offerModal && !its_offerBtn && offer_is_active) {
      offerPriceShow();
    }
  });
}

// modal window checkout-modal // 
const checkModal = document.querySelector('.checkout-modal');
const checkOpenButton = document.querySelector('.information-order__button');
const checkCloseButton = document.querySelector('.checkout__close');
const checkTabs = document.querySelectorAll('.checkout__tab');
const checkoutContent = document.querySelectorAll('.check-content');

if (checkoutContent.length > 0 || checkTabs.length > 0) {

    function hideTabContent() {
      checkoutContent.forEach(item => {
            item.classList.remove('check-active');
        });

        checkTabs.forEach(item => {
          item.classList.remove('check-active');
      });
    }

    function showTabContent(i = 0) {
        checkoutContent[i].classList.add('check-active');
        checkTabs[i].classList.add('check-active');
    }

    hideTabContent();
    showTabContent();

    checkTabs.forEach((tab, index) => {
        tab.addEventListener('click', () => {
            hideTabContent();
            showTabContent(index);
        });
    });
}

if (mainContent.classList.contains("basket__content")) {
  const checkModalOpen = function () {
    checkModal.classList.add('checkout-modal--active');
  }

  const checkModalClose = function () {
    checkModal.classList.remove('checkout-modal--active');
  }

  checkOpenButton.addEventListener('click', function() {
    checkModalOpen();
    overlaAсtive();
  })

  checkCloseButton.addEventListener('click',function() {
    checkModalClose();
    overlaDelete();
  })

  const sendOrderRequestButton = document.querySelector('.send-order-request__button');
  const orderPlacedModal = document.querySelector('.order-placed-modal');
  const orderPlacedClose = document.querySelector('.order-placed-modal__close');

  const sendOrderRequestShow = function () {
    orderPlacedModal.classList.add('order-placed-modal--show');
  }

  const sendOrderRequestClose = function () {
    orderPlacedModal.classList.remove('order-placed-modal--show');
  }

  sendOrderRequestButton.addEventListener('click', function () {
    sendOrderRequestShow();
    checkModalClose();
  });

  orderPlacedClose.addEventListener('click', function () {
    sendOrderRequestClose();
    overlaDelete();
  });
}

// cabinet page
// favourites 
// orders & history 
var acc = document.querySelectorAll(".my-order__button");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.innerText='Свернуть';
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
      this.innerText='Подробнее';
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

// about company
// modal 
  const callbackModal = document.querySelector('.callback__modal');
  const callbackSubModal = document.querySelector('.callback-modal__submitted');
  const callbackOpen = document.querySelectorAll('.principles__button');
  const callbackClose = document.querySelector('.callback-modal__close');
  const callbackSubmit = document.querySelector('.callback-modal__button');
  const callbackSubClose = document.querySelector('.callback-modal-submitted__close');

  const modalCallOpen = function() {
    callbackModal.classList.add('callback__modal--active');
  }; 

  const modalCallClose = function() {
    callbackModal.classList.remove('callback__modal--active');
  };

  const callbackSubModalOpen = function () {
    callbackSubModal.classList.add('callback-modal__submitted--active');
  }

  const callbackSubModalClose = function () {
    callbackSubModal.classList.remove('callback-modal__submitted--active');
  }

  callbackOpen.forEach((open) => {
    open.addEventListener('click', function() {
      modalCallOpen();
      overlaAсtive();
    });
  });
  
  callbackClose.addEventListener('click', function() {
    modalCallClose();
    overlaDelete();
  });

  callbackSubmit.addEventListener('click', function() {
    modalCallClose();
    callbackSubModalOpen();
  })

  callbackSubClose.addEventListener('click', function() {
    callbackSubModalClose();
    overlaDelete();
  })

if (mainContent.classList.contains('registration__content')) {
  const sentRegButton = document.querySelector('.registration__button');
  const successfullyModal = document.querySelector('.successfully-modal');
  const closeRegButton = document.querySelector('.successfully-modal__close');

  const sentRegistration = function () {
    successfullyModal.classList.add('successfully-modal--show');
  }

  const closeRegistration = function() {
    successfullyModal.classList.remove('successfully-modal--show');
  }

  sentRegButton.addEventListener('click', function () {
    sentRegistration();
  });

  closeRegButton.addEventListener('click', function() {
    closeRegistration();
  });
}

// changesSaved // 
saveChangesButton = document.querySelectorAll('.save-changes__button');

saveChangesButton.forEach((changes) => {
  changes.addEventListener('click', function () {
    changes.classList.add('changes-saved');
  });
});






