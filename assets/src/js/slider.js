// Swiper Slider Settings // 

// Banner Slider //

var swiper = new Swiper(".banner__slider", {
    autoplay: true,
    spaceBetween: 10,
    slidesPerView: 'auto',
    loop: true,
    delay: 3000,
    lazy: {
        loadPrevNext: true,
    },

    pagination: {
      el: ".banner__slider-pagination",
    },

    navigation: {
        nextEl: '.swiper-custom-button-next',
        prevEl: '.swiper-custom-button-prev',
    },
    
    breakpoints: {
        375: {
            spaceBetween: 10
        },

        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1910: {
            spaceBetween: 35
        }
    }
});

// Product Slider // 

var swiper = new Swiper(".seafood__slider", {
    autoplay: true,
    loop: true,
    delay: 2900,
    slidesPerView: 'auto',
    spaceBetween: 20,
    lazy: {
        loadPrevNext: true,
    },

    pagination: {
      el: ".seafood__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.seafood-slider-next',
        prevEl: '.seafood-slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1910: {
            spaceBetween: 35
        }
    }
});

var swiper = new Swiper(".meatfood__slider", {
    autoplay: true,
    loop: true,
    delay: 2800,
    slidesPerView: 'auto',
    spaceBetween: 20,
    pagination: {
      el: ".meatfood__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.meetfood-slider-next',
        prevEl: '.meetfood-slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1909: {
            spaceBetween: 35
        }
    }
});

// Recipes Slider // 

var swiper = new Swiper(".recipes__slider", {
    autoplay: true,
    loop: true,
    delay: 2700,
    slidesPerView: 'auto',
    spaceBetween: 20,

    pagination: {
      el: ".recipes__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.recipes__slider-next',
        prevEl: '.recipes__slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30,
            grid: {
                rows: 1,
            },
        },

        1901: {
            spaceBetween: 35,
            grid: {
                rows: 2,
            },
        }
    }
});


// Аrticles Slider //

var swiper = new Swiper(".articles__slider", {
    autoplay: true,
    loop: true,
    delay: 3000,
    slidesPerView: 'auto',
    spaceBetween: 20,
    pagination: {
      el: ".articles__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.articles__slider-next',
        prevEl: '.articles__slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1910: {
            spaceBetween: 35,
            grid: {
                rows: 1,
            },
        }
    }
});

// Partners Slider //

var swiper = new Swiper(".partners__slider", {
    autoplay: true,
    loop: true,
    delay: 3000,
    slidesPerView: 'auto',
    spaceBetween: 20,
    pagination: {
      el: ".partners__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.partners-slider-next',
        prevEl: '.partners-slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1910: {
            spaceBetween: 35
        }
    }
});

// Recipes Slider // 

var swiper = new Swiper(".principles__slider", {
    autoplay: true,
    loop: true,
    delay: 2700,
    slidesPerView: 'auto',
    spaceBetween: 20,

    pagination: {
      el: ".principles__slider-pagination",
      clickable: true,
    },

    navigation: {
        nextEl: '.principles__slider-next',
        prevEl: '.principles__slider-prev',
    },

    breakpoints: {
        576: {
            spaceBetween: 20
        },

        768: {
            spaceBetween: 30
        },

        1910: {
            spaceBetween: 35
        }
    }
});

// Carousel Card Slider // 

var swiper = new Swiper(".minicarouselSlider", {
    loop: true,
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
    pagination: {
      el: ".minicarouse__slider-pagination",
      clickable: true,
    },

    breakpoints: {
        992: {
            direction: 'horizontal',
        },

        768: {
            slidesPerView: 3,
            direction: 'vertical',
        },

        576: {
            direction: 'horizontal',

        }
    }
});

var swiper2 = new Swiper(".maxcarouselSlider", {
    loop: true,
    spaceBetween: 10,
    thumbs: {
        swiper: swiper,
    },
});

// Fancybox Carousel // 

Fancybox.bind('[data-fancybox="gallery"]', {
    Thumbs: false,
    Toolbar: false,
  
    Image: {
      zoom: false,
      click: false,
      wheel: "slide",
    },
});