// Global variables // 
const mainsContent = document.querySelector(".main__content");

if (mainsContent.classList.contains("contacts__content")) {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                center: [55.751574, 37.573856],
                zoom: 9
            }, {
                searchControlProvider: 'yandex#search'
            }),
    
            // Создаём макет содержимого.
            MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            ),
    
            moscowPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'Spgfood.ru в Москве',
                // balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40]
            }),
    
            khimkiPlacemark = new ymaps.Placemark([55.897, 37.4297], {
                hintContent: 'Spgfood.ru в Химки',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
            zhukovPlacemark = new ymaps.Placemark([55.03178, 36.74402], {
                hintContent: 'Spgfood.ru в Жукове',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
            korolevPlacemark = new ymaps.Placemark([55.9142, 37.8256], {
                hintContent: 'Spgfood.ru в Королеве',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
            zelenogradPlacemark = new ymaps.Placemark([55.9825, 37.1814], {
                hintContent: 'Spgfood.ru в Зеленограде',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
            lyubertsyPlacemark = new ymaps.Placemark([55.6772, 37.8932], {
                hintContent: 'Spgfood.ru в Люберцах',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
            odintsovoPlacemark = new ymaps.Placemark([55.678, 37.2777], {
                hintContent: 'Spgfood.ru в Одинцово',
                // balloonContent: 'Химки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'img/icon/marker-map.svg',
                // Размеры метки.
                iconImageSize: [80, 80],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-40, -40],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });
    
        myMap.geoObjects
            .add(moscowPlacemark)
            .add(khimkiPlacemark)
            .add(korolevPlacemark)
            .add(zhukovPlacemark)
            .add(zelenogradPlacemark)
            .add(lyubertsyPlacemark)
            .add(odintsovoPlacemark);
    });
    
    const cityTabs = document.querySelectorAll('.city__button');
    const cityContent = document.querySelectorAll('.addresses__block');
    
    if (cityContent.length > 0 || cityTabs.length > 0) {
    
        function hideCityContent() {
            cityContent.forEach(item => {
                item.classList.remove('city--active');
            });
    
            cityTabs.forEach(item => {
              item.classList.remove('city--active');
          });
        }
    
        function showCityContent(i = 0) {
            cityContent[i].classList.add('city--active');
            cityTabs[i].classList.add('city--active');
        }
    
        hideCityContent();
        showCityContent();
    
        cityTabs.forEach((tab, index) => {
            tab.addEventListener('click', () => {
                hideCityContent();
                showCityContent(index);
            });
        });
    }

}