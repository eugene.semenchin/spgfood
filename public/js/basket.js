const mainContents = document.querySelector(".main__content");
const productsBtn = document.querySelectorAll('.add-to-basket__button');
const cartProductsList = document.querySelector('.basket-card__list');
const cart = document.querySelector('.header__basket');
const cartQuantity = document.querySelector('.quantity-basket');
const fullPrice= document.querySelector('.fullprice');
const fullWeight = document.querySelector('.fullweight')
let price = 0;
let weight = 0;

function num_word(value, words) {  
	value = Math.abs(value) % 100; 
	var num = value % 10;
	if(value > 10 && value < 20) return words[2]; 
	if(num > 1 && num < 5) return words[1];
	if(num == 1) return words[0]; 
	return words[2];
}


const cartModule = {
    'count': 0,
    
    'total': 0,

    'heft': 0,

    'list': {},

    update: function() {
        const localStorage = this.getLocalStorage();

        if (localStorage) {
            this.count = localStorage.count;
            this.total = localStorage.total;
            this.heft = localStorage.heft;
            this.list = localStorage.list;
        }

        this.changeMiniCartCount();
        this.updateCartList();
    },

    getLocalStorage: function () {
        return window.localStorage.getItem('cart') 
            ? JSON.parse(window.localStorage.getItem('cart'))
            : false
    },

    setLocalStorge: function () {
        window.localStorage.setItem('cart', JSON.stringify({
            count: this.count,
            total: this.total,
            heft: this.heft,
            list: this.list
        }));
    },

    updateCount: function () {
        this.count = 0;
        for (let id in this.list) {
            this.count += this.list[id]['quantity']
        }
    },

    updateTotal: function () {
        this.total = 0;
        for (let id in this.list) {
            this.total += this.list[id]['total']
        }
    },

    updateHeft: function () {
        this.heft = 0;
        for (let id in this.list) {
            this.heft += this.list[id]['heft']
        }
    },
   
    add: function (id, price, title, image, weight, quantity) {
        console.log(id, price, title, image, weight, quantity);
        let product_quantity = quantity;
    
        if (id in this.list) {
            product_quantity = this.list[id]['quantity'] + quantity;
        }
        
        this.list[id] = {
            'id': id, 
            'price': price,
            'weight': weight,
            'title': title,
            'image': image,
            'quantity': product_quantity,
            'total': price * product_quantity,
            'heft': weight * product_quantity
        }

        this.updateCount();
        this.updateTotal();
        this.updateHeft();
        this.setLocalStorge();
        this.update();
    },

    remove: function (id) {
        delete this.list[id];
        
        this.updateCount();
        this.updateTotal();
        this.updateHeft();
        this.setLocalStorge();
        this.update();
    },

    increment: function (id) {
        this.list[id].quantity += 1;
        this.list[id].total = this.list[id].quantity * this.list[id].price;
        this.list[id].heft = this.list[id].quantity * this.list[id].weight;
        this.updateCount();
        this.updateTotal();
        this.updateHeft();
        this.setLocalStorge();
        this.update();
    },
    
    decrement: function (id) {
        this.list[id].quantity -= 1;
        if (this.list[id].quantity === 0) {
            this.remove(id)
        } else {
            this.list[id].total = this.list[id].quantity * this.list[id].price;
            this.list[id].heft = this.list[id].quantity * this.list[id].weight;
        }
        this.updateCount();
        this.updateTotal();
        this.updateHeft();
        this.setLocalStorge();
        this.update();
    },

    clear: function () {
        this.count = 0;
        this.total = 0;
        this.heft = 0;
        this.list = {};

        this.changeMiniCartCount();
        this.updateCartList();
        this.setLocalStorge();
    },

    changeMiniCartCount() {
        document.querySelector('.quantity-basket').textContent = this.count;
        document.querySelector('.mobile-basket__quantity').textContent = this.count;
    },
    
    updateCartList() {
        
            let cartHtml = '';
        
            for (let productId in this.list) {
                let product = this.list[productId];
                cartHtml += this.templateMiniCartProduct(product);
            }
            
            if (mainContents.classList.contains("basket__content")) {
                document.querySelector('.fullprice').textContent = this.total;
                document.querySelector('.fullweight').textContent = this.heft;
                document.querySelector('.order__amount').textContent = `${this.count} ${num_word(this.count, ['товар', 'товара', 'товаров'])}`;
                document.querySelector('.basket-card__list').innerHTML = cartHtml;

                
            }
        setRemoveProductFromCartEvents();
    },

    templateMiniCartProduct: (product) => {
        return `
                <div class="basket-card__item js-mini-cart-item" data-id="${product.id}">
                <div class="basket-card__image-wrapper">
                    <img class="basket-card__image" src="${product.image}" alt="card-image">
                </div>
                <div class="basket-card__content">
                    <button class="basket-card-close__button js-remove-from-cart">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.5364 0.263604C1.18492 -0.087868 0.615076 -0.087868 0.263604 0.263604C-0.087868 0.615076 -0.087868 1.18492 0.263604 1.5364L2.72721 4L0.263605 6.4636C-0.0878671 6.81508 -0.0878671 7.38492 0.263605 7.7364C0.615077 8.08787 1.18492 8.08787 1.5364 7.7364L4 5.27279L6.4636 7.7364C6.81508 8.08787 7.38492 8.08787 7.7364 7.7364C8.08787 7.38492 8.08787 6.81508 7.7364 6.4636L5.27279 4L7.7364 1.5364C8.08787 1.18492 8.08787 0.615076 7.7364 0.263604C7.38492 -0.087868 6.81508 -0.087868 6.4636 0.263604L4 2.72721L1.5364 0.263604Z" fill="#78909C"/>
                        </svg>
                        <span>Удалить</span>
                    </button>
                    <h5 class="basket-card__title">${product.title}</h5>
                    <div class="basket-card__control">
                        <div class="management__quantity">
                            <button class="quantity__button js-cart-minus" data-id="minus">
                                <svg class="quantity__icon" width="18" height="2" viewBox="0 0 18 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 1C0 0.447715 0.447715 0 1 0H17C17.5523 0 18 0.447715 18 1C18 1.55228 17.5523 2 17 2H1C0.447716 2 0 1.55228 0 1Z" fill="#78909C"/>
                                </svg>
                            </button>
                            <span class="quantity__input">${product.quantity}</span>
                            <button class="quantity__button js-cart-plus" data-id="plus">
                                <svg class="quantity__icon" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 6C0 5.44772 0.447715 5 1 5H11C11.5523 5 12 5.44772 12 6C12 6.55228 11.5523 7 11 7H1C0.447715 7 0 6.55228 0 6Z" fill="#78909C"/>
                                    <path d="M6 0C6.55228 2.41411e-08 7 0.447715 7 1L7 11C7 11.5523 6.55228 12 6 12C5.44771 12 5 11.5523 5 11L5 1C5 0.447715 5.44772 -2.41411e-08 6 0Z" fill="#78909C"/>
                                    </svg>
                            </button>
                        </div>
                        <div class="information__price">
                            <div class="piece__price">
                            ${product.weight}
                                <span>кг</span>
                            </div>
                            <div class="total__price">
                                ${product.price}
                                <span>₽</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}

cartModule.update();

document.querySelectorAll('.js-add-to-cart').forEach(el => {
    el.addEventListener('click', (e) => {
        let self = e.currentTarget;

        console.log('js-add-to-cart CLICK', self.dataset);
        cartModule.add(
            self.dataset.id,
            self.dataset.price,
            self.dataset.title,
            self.dataset.image,
            self.dataset.weight,
            1
        )
        
    });
});


function setRemoveProductFromCartEvents () {
    document.querySelectorAll('.js-remove-from-cart').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.remove(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-cart-minus').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.decrement(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-cart-plus').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.increment(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-return-to-cart').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
        });
    });
}

if (mainContents.classList.contains("basket__content")) {
    document.querySelector('.js-clear-cart').addEventListener('click', (e) => {
        cartModule.clear()
    })
}